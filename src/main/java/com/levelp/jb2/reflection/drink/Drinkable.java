package com.levelp.jb2.reflection.drink;

public @interface Drinkable {

    public boolean isValid();
}
