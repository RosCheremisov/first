package com.levelp.jb2.basics;

import java.util.Scanner;

/**
 * 1. Ввести с консоли количество строк
 * 2. Ввести с консоли эти строки
 * 3. В каждой строке если содержится слово lol - тогда вывести это слово
 * 4. Если слово заканчивается на ll - то вывести 2 первых символа
 * 5. Если ничего из этого - то вывести слово если символ 't' находится на 3-й позиции, иначе вывести - "Бесполезное слово"
 */
public class StringConditions {

    private final static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        // 1
        int amountOfStrings = getAmountOfWords();
        // 2
        String[] stringArray = createArray(amountOfStrings);
        for (String word : stringArray) {
            // 3
            if (!isLolWord(word) && !isWordsWithLL(word)) {
                printWordWithTOnThirdPosition(word);
            }
        }
    }

    private static void printWordWithTOnThirdPosition(String word) {
        if (word.indexOf('t') == 3) {
            System.out.println("Word with t on third position - " + word);
            System.out.println();
        } else {
            System.out.println("Бесполезное слово - " + word);
            System.out.println();
        }
    }

    private static boolean isLolWord(String word) {
        if (word.contains("lol")) {
            System.out.println("Next word contains lol - " + word);
            System.out.println();
            return true;
        }
        return false;
    }

    private static boolean isWordsWithLL(String word) {
        if (word.endsWith("ll")) {
            System.out.println("Word contains ll - " + word.substring(0, 2));
            System.out.println();
            return true;
        }
        return false;
    }

    private static int getAmountOfWords() {
        System.out.println("Please input amount of words");
        return scanner.nextInt();
    }

    /**
     * Creates array of lines from console
     */
    private static String[] createArray(int amountOfStrings) {
        String[] stringArray = new String[amountOfStrings];
        for (int i = 0; i < amountOfStrings; ++i) {
            System.out.println("Please input next word: ");
            stringArray[i] = scanner.next();
        }
        System.out.println();
        return stringArray;
    }
}
