package com.levelp.jb2.basics;

import java.util.Scanner;

/**
 * Нужно ввести x - кол-во элементов по оси x
 * Нужно ввести y - кол-во элементов по оси y
 *
 *   2 - 3
 *   Нужно ввести массив
 *
 *   Будет выглядеть
 *   [4, 3]
 *   [5, 2]
 *   [1, 5]
 *
 *   1. Найти минимальный элемент
 *   2. Заменить все числа меньше 0 на 0 и вывести массив
 *
 */
public class TwoDimensionalArrays {

    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        int x = getAmount("x axis");
        int y = getAmount("y axis");
        int [][] array = getArray(x, y);
        printArray(array);
        System.out.println("Minimal element is " + getMin(array));
        clearNegative(array);
    }

    private static int getMin(int[][] array) {
        int min = array[0][0];
        for (int i = 0; i < array.length; ++i) {
            for (int j = 0; j < array[i].length; j++) {
                if (min > array[i][j]) {
                    min = array[i][j];
                }
            }
        }
        return min;
    }

    private static void clearNegative(int[][] array) {
        System.out.println("Results before cleaning up of negative values");
        printArray(array);
        for (int i = 0; i < array.length; ++i) {
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] < 0) {
                    array[i][j] = 0;
                }
            }
        }
        System.out.println("Results after cleaning up of negative values");
        printArray(array);
    }

    private static int[][] getArray(int x, int y) {
        int [][] array = new int[x][y];
        for (int i = 0; i < x; ++i) {
            for (int j = 0; j < y; j++) {
                array[i][j] = getAmount("next array value");
            }
        }
        return array;
    }

    private static int[][] printArray(int[][] array) {
        for (int i = 0; i < array.length; ++i) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        return array;
    }

    private static int getAmount(String description) {
        System.out.println("Please input " + description);
        return SCANNER.nextInt();
    }
}
