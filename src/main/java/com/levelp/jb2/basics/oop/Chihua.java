package com.levelp.jb2.basics.oop;

public class Chihua extends Dog {

    public Chihua() {
        super(12, 12);
    }

    @Override
    String getDogName() {
        return "Chihua";
    }
}
