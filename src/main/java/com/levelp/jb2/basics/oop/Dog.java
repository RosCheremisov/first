package com.levelp.jb2.basics.oop;

/**
 * Есть 3 породы собак (Бишон, Ротвейлер, Чихуахуа)
 * Каждая собака умеет лаять,кусаться и какать (функции), у каждой собаки есть характеристки (уровень аггресии и уровень дружелюбности)
 * При аггреси > 3 - лаять вывести Бешеная собака,иначе - Хороший пес не лает
 * При аггреси > 5 - кусаться вывести Бешеная собака укусила ребенка,иначе - Хороший пес не лает,ааа лижется
 *  При друж > 5 и а < 3- какать вывести Отлично идет,иначе - СОбака обосрака
 * Бишон -  (а - 1,д - 8)
 * Ротвейлер (5,4)
 * Чихуа - (12, 12)
 */
public abstract class Dog {

    protected int aggressionLevel;
    protected int friendlyLevel;

    public Dog(int aggressionLevel, int friendlyLevel) {
        this.aggressionLevel = aggressionLevel;
        this.friendlyLevel = friendlyLevel;
    }

    void bark() {
        printType();
        if (aggressionLevel > 3) {
            System.out.println("Бешеная собака");
        } else {
            System.out.println("Хороший пес не лает");
        }
    }

    void bite() {
        printType();
        if (aggressionLevel > 5) {
            System.out.println("Бешеная собака укусила ребенка");
        } else {
            System.out.println("Хороший пес не лает");
        }
    }

    void defecate() {
        printType();
        if (aggressionLevel < 3 && friendlyLevel > 5) {
            System.out.println("Отлично идет");
        } else {
            System.out.println("СОбака обосрака");
        }
    }

    abstract String getDogName();

    private void printType() {
        System.out.println("Это порода - " + getDogName());
    }
}
