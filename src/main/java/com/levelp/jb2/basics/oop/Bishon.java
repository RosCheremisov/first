package com.levelp.jb2.basics.oop;

public class Bishon extends Dog {

    public Bishon() {
        super(1, 8);
    }

    @Override
    String getDogName() {
        return "Bishon";
    }
}
