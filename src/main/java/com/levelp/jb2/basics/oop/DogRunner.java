package com.levelp.jb2.basics.oop;

public class DogRunner {

    public static void main(String[] args) {
        DogRunner dogRunner = new DogRunner();
        dogRunner.testDog(new Chihua());
    }

    private void testDog(Dog dog) {
        dog.bark();
        dog.bite();
        dog.defecate();
    }
}
