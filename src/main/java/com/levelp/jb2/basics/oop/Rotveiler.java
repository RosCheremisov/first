package com.levelp.jb2.basics.oop;

public class Rotveiler extends Dog {

    public Rotveiler() {
        super(5, 4);
    }

    @Override
    String getDogName() {
        return "Rotveiler";
    }
}
