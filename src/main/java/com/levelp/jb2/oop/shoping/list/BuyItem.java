package com.levelp.jb2.oop.shoping.list;

import com.levelp.jb2.util.InputUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class BuyItem {
    private String name;
    private int amount;
    private double value;

    public static BuyItem inputBuyItemField(){
        return BuyItem.builder().name( InputUtil.nextString("Введите название товара: "))
                .amount(InputUtil.nextInt("Введите количество товара: "))
                .value(InputUtil.nextDouble("Введите сумму товара: "))
                .build();
    }
}
