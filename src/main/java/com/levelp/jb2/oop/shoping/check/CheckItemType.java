package com.levelp.jb2.oop.shoping.check;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum CheckItemType {
    FRUITS(1),
    VEGETABLES(2),
    MILK(3),
    CANDY(4),
    CEREALS(5);

    private final int inputType;

    public static CheckItemType find(int inputType) {
        for (CheckItemType value : values()) {
            if (value.inputType == inputType) {
                return value;
            }
        }
        throw new RuntimeException("Не верный ввод");
    }

}
