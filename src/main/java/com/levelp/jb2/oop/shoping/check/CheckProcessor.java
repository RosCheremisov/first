package com.levelp.jb2.oop.shoping.check;

import com.levelp.jb2.util.InputUtil;
import static com.levelp.jb2.oop.shoping.check.CheckItemType.*;

public class CheckProcessor {
    private final BuyItemRepository checkShoppingList = new BuyItemRepository();

    public void run(){
        String starting = InputUtil.nextString("Приступить к покупке: yes or no");
        while (starting.equalsIgnoreCase("yes")) {
            checkShoppingList.add(CheckItem.create());
            starting = InputUtil.nextString("Продолжит покупку: yes or no");
        }
        for (CheckItemType type : values()) {
            checkShoppingList.sumTypeList(type);
        }
    }
}
