package com.levelp.jb2.oop.shoping.list;

import lombok.ToString;

import java.util.Arrays;

@ToString
public class BuyItemRepository {
    int size = 3;
    private BuyItem[] list = new BuyItem[size];

    public void add(BuyItem a) {
        for (int i = 0; i < list.length; i++) {
            if (list[list.length - 1] != null) {
                list = Arrays.copyOf(list, list.length * 2);
            } else if (list[i] == null) {
                list[i] = a;
                break;
            }
        }
    }

    public void printAll() {
        for (int i = 0; i < list.length - 1; i++) {
            System.out.println(list[i]);
        }
    }

    public void printLastFive() {
        if (size > 5) {
            for (int i = list.length - 6; i < list.length - 1; i++) {
                System.out.println(list[i]);
            }
        } else {
            for (int i = 0; i < list.length - 1; i++) {
                System.out.println(list[i]);
            }

        }

    }
}
