package com.levelp.jb2.oop.shoping.check;

/**
 * Нужно собрать чек со списками покупок - название, тип, кол-во, сумма за единицу.
 * Вводится каждея единица с консоли - в итоге вывести весь чек и итоговую сумму.
 * Типы продукции - фрукты, овощи, молочка, сладости, крупы
 */

public class CheckShoppingListRunner {
    public static void main(String[] args) {
        CheckProcessor a = new CheckProcessor();
        a.run();
    }
}
