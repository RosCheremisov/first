package com.levelp.jb2.oop.shoping.list;

import com.levelp.jb2.util.InputUtil;

public class BuyProcessor {

    private final BuyItemRepository shoppingList = new BuyItemRepository();

    public void run() {
        String starting = InputUtil.nextString("Хотите добавить покупку : yes или no");
        while (starting.equalsIgnoreCase("yes")) {
            shoppingList.add(BuyItem.inputBuyItemField());
            String inputPrint = InputUtil.nextString("Для вывода всего списка покупок введите: all; Для вывода последних пять: five; Для продолжения любую кнопку");
            if (inputPrint.equalsIgnoreCase("all")) {
                shoppingList.printAll();
            } else if (inputPrint.equalsIgnoreCase("five")) {
                shoppingList.printLastFive();
            }
            starting = InputUtil.nextString("Хотите еще добавить покупку : yes или no");
        }
    }
}

