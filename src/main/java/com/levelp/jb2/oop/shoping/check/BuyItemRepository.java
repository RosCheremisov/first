package com.levelp.jb2.oop.shoping.check;

import lombok.ToString;

import java.util.*;

@ToString
public class BuyItemRepository {
    private final Map<CheckItemType,List<CheckItem>> productList = new HashMap<>();

    public void add(CheckItem item){
        if(productList.containsKey(item.getType())){
            productList.get(item.getType()).add(item);
        }else {
            List<CheckItem> typeList = new ArrayList<>();
            typeList.add(item);
            productList.put(item.getType(), typeList);
        }
    }

    public void print(){
        for(Map.Entry<CheckItemType,List<CheckItem>> entry: productList.entrySet()){
            System.out.println("Тип: " + entry.getKey() + " Товар: " + entry.getValue());
        }
    }

    public void sumTypeList(CheckItemType key){
        int sum = 0;
        for(Map.Entry<CheckItemType,List<CheckItem>> entry: productList.entrySet()){
            if(productList.containsKey(key)){
                for(CheckItem buyItem: entry.getValue()){
                    if (buyItem.getType() == key){
                        sum += buyItem.getValue();
                    }
                }
                System.out.println("Сумма за: " + sum + " грн");
            }
        }
    }
}
