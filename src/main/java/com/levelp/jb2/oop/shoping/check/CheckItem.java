package com.levelp.jb2.oop.shoping.check;

import com.levelp.jb2.util.InputUtil;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Arrays;
import static com.levelp.jb2.oop.shoping.check.CheckItemType.*;

@Data
@NoArgsConstructor
public class CheckItem {
    private String name;
    private CheckItemType type;
    private int amount;
    private int value;

    public static CheckItem create() {
        CheckItem checkItem = new CheckItem();
        checkItem.setName(InputUtil.nextString("Введите название товара: "));
        System.out.println(Arrays.toString(values()));
        checkItem.setType(find(InputUtil.nextInt("Выбирете тип товара из перечисленых: ")));
        checkItem.setAmount(InputUtil.nextInt("Введите количество товара: "));
        checkItem.setValue(InputUtil.nextInt("Введите сумму товара: "));
        return checkItem;
    }
}
