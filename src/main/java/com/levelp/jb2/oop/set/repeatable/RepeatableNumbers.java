package com.levelp.jb2.oop.set.repeatable;
/**
 * Пользователь вводит в строке набор чисел в одной строке 1, 1,2,3,3,3,4,5
 * - избавиться от повторяющихся чисел на экране и вывести результат - сделать через коллекции
 */

import com.levelp.jb2.util.InputUtil;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class RepeatableNumbers {
    public static void main(String[] args) {
        String str = InputUtil.nextString("Введите числа через пробел");
        addNumbers(str);
    }

    public static void addNumbers(String str) {
        Set<String> numbersSet = new HashSet<String>(Arrays.asList(str.split(" ")));
        for (String number : numbersSet) {
            System.out.println(number);
        }
    }
}
