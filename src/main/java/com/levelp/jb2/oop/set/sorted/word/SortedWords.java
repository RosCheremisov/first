package com.levelp.jb2.oop.set.sorted.word;

import com.levelp.jb2.util.InputUtil;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

/**
 *На вход подается слово - может быть много, нужно хранить слова в отсортированном виде и выводить за минимальое кол-во операций/времени
 */

public class SortedWords {
    public static void main(String[] args) {
        String str = InputUtil.nextString("Введите слово или слова через запятую: ");
        addWords(str);
    }

    public static void addWords(String str) {
        Set<String> sortedList = new TreeSet<String>(Arrays.asList(str.split(",")));
        for (String word : sortedList) {
            System.out.println(word);
        }
    }
}
