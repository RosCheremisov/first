package com.levelp.jb2.oop.queue;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Queue {
    private Node head = null;
    private Node tail = null;
    private int size;

    public void add(int a){
        Node node = new Node();
        node.setElement(a);
        if(head == null){
            head = node;
        } else {
            tail.setNext(node);
        }
        tail = node;
        size++;
    }

    public int remove() {
        if (size == 0) {
            return 0;
        }
        int element = head.getElement();
        head = head.getNext();
        if(head == null){
            tail = null;
        }
        size--;
        return element;
    }

    public boolean hasNext(Node node){
        if(node.getNext() == null){
            return false;
        }
        return true;
    }

    public void printList(){
        Node t = head;
        while (hasNext(t)){
            System.out.print(" " + t.getElement());
            t = t.getNext();
        }
        System.out.println();
    }
}


