package com.levelp.jb2.oop.queue;

public class QueueRunner {
    public static void main(String[] args) {
        Queue queue = new Queue();
        queue.add(10);
        queue.add(20);
        queue.add(30);
        queue.add(40);
        queue.add(50);
        queue.add(60);
        queue.printList();
        queue.remove();
        System.out.println("После удаления");
        queue.printList();
    }
}
