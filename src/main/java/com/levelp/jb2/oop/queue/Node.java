package com.levelp.jb2.oop.queue;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Node {
    private Node next;
    private int element;
}
