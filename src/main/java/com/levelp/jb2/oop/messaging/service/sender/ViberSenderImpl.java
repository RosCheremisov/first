package com.levelp.jb2.oop.messaging.service.sender;

import com.levelp.jb2.oop.messaging.model.Message;
import java.util.Base64;

public class ViberSenderImpl implements Sender {

    @Override
    public boolean send(Message message) {
        System.out.println("Sending message to VIBER");
        byte[] encodedMessageBytes = Base64.getEncoder().encode(message.getMessage().getBytes());
        String encodeMessage = new String(encodedMessageBytes);
        System.out.println("Message: " + encodeMessage);
        System.out.println("Sender: " + message.getSender());
        return true;
    }
}
