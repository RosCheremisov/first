package com.levelp.jb2.oop.messaging.service.sender;

import com.levelp.jb2.oop.messaging.model.Message;

public interface Sender {

    /**
     * This method sends Message
     * @param message - Message which will be sent
     * @return Boolean - if message is sent true, otherwise false
     */
    boolean send(Message message);

}
