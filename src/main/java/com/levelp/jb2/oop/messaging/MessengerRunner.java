package com.levelp.jb2.oop.messaging;

import com.levelp.jb2.oop.messaging.model.Message;
import com.levelp.jb2.oop.messaging.service.SenderService;
import com.levelp.jb2.oop.messaging.service.sender.Sender;
import com.levelp.jb2.oop.messaging.service.sender.SkypeSenderImpl;
import com.levelp.jb2.oop.messaging.service.sender.TelegramSenderImpl;
import com.levelp.jb2.oop.messaging.service.sender.ViberSenderImpl;
import java.util.Scanner;

public class MessengerRunner {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String messageBody = scanner.nextLine();
        String sender = scanner.nextLine();
        Message message = new Message(messageBody, sender);

        SenderService service = new SenderService();
        int messengerNum = scanner.nextInt();
        Sender innerSender;
        switch (messengerNum) {
            case 1: {
                innerSender = new TelegramSenderImpl();
                break;
            } case 2: {
                innerSender = new ViberSenderImpl();
                break;
            } default: {
                innerSender = new SkypeSenderImpl();
            }
        }


        service.process(innerSender, message);
    }
}
