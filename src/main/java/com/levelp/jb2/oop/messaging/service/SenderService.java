package com.levelp.jb2.oop.messaging.service;

import com.levelp.jb2.oop.messaging.model.Message;
import com.levelp.jb2.oop.messaging.service.sender.Sender;

public class SenderService {

    public void process(Sender sender, Message message) {
        if (sender.send(message)) {
            System.out.println("Successfully sent message");
        } else {
            System.out.println("Shit. We've got problem.");
        }
    }
}
