package com.levelp.jb2.oop.messaging.service.sender;

import com.levelp.jb2.oop.messaging.model.Message;

public class TelegramSenderImpl implements Sender {

    @Override
    public boolean send(Message message) {
        System.out.println("Sending message to telegram...");
        System.out.println(message);
        return true;
    }
}
