package com.levelp.jb2.oop.person;

/**
 * "Создать класс Person, который содержит:
 * a) поля fullName, age.
 * б) методы move() и talk(), в которых просто вывести на консоль сообщение -""Такой-то  Person говорит"".
 * в) Добавьте два конструктора  - Person() и Person(fullName, age).
 * Создайте два объекта этого класса. Один объект инициализируется конструктором Person(), другой - Person(fullName, age)."
 */

public class Person {
    private String fullName;
    private int age;

    public Person(String fullName, int age) {
        this.fullName = fullName;
        this.age = age;
    }

    public Person() {

    }

    public void talk() {
        System.out.println("говорит сейчас " + fullName + " ему: " + age);
    }

    public static void main(String[] args) {
        Person rostik = new Person("Ростик", 28);
        Person valera = new Person();
        valera.setFullName("Валера");
        valera.setAge(33);
        rostik.talk();
        valera.talk();
        System.out.println("Валера " + valera.getFullName());
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
