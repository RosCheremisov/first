package com.levelp.jb2.oop.car;

import com.levelp.jb2.oop.car.repository.CarDataBase;
import com.levelp.jb2.util.InputUtil;

public class CarService {

    CarDataBase dataBase = new CarDataBase();

    public void run (){
        dataBase.printOperation();
        int starting = (InputUtil.nextInt("Выбирете нужную вам операцию: "));
        while (starting != 0){
            switch (starting){
                case 1:
                    dataBase.add();
                    break;
                case 2:
                    dataBase.groupCarByBrand(InputUtil.nextString("Введите название производителя: "));
                    break;
                case 3:
                    dataBase.printAllCar();
                    break;
                case 4:
                    dataBase.printAllCarByBrand(InputUtil.nextString("Введите название производителя: "));
                    break;
                default:
                    throw new RuntimeException("Неверный ввод");
            }
            dataBase.printOperation();
            starting = InputUtil.nextInt("Выбирете нужную вам операцию: ");
        }
    }
}
