package com.levelp.jb2.oop.car.model;

import com.levelp.jb2.util.InputUtil;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
public class Car {
    private String brand;
    private String model;
    private String color;
    private int value;

    public static Car createDataCar(){
        return Car.builder().brand(InputUtil.nextString("Введите марку машины: "))
                .model(InputUtil.nextString("Введите название модели машины: "))
                .color(InputUtil.nextString("Введите цвет машины: "))
                .value(InputUtil.nextInt("Введите стоимость машины"))
                .build();
    }
}
