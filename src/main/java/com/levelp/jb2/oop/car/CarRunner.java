package com.levelp.jb2.oop.car;

public class CarRunner {
    public static void main(String[] args) {
        CarService service = new CarService();
        service.run();
    }
}
