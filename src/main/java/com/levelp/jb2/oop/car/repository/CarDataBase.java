package com.levelp.jb2.oop.car.repository;

import com.levelp.jb2.oop.car.model.Car;

import java.util.*;
import java.util.stream.Collectors;

public class CarDataBase {

    private final Set<Car> carSet = new HashSet<Car>();

    public void add(){
        carSet.add(Car.createDataCar());
    }

    public void groupCarByBrand (String brand){
        carSet.stream().filter(car -> car.getBrand().equalsIgnoreCase(brand))
                       .peek(System.out::println)
                       .collect(Collectors.toList());
    }

    public void printAllCarByBrand(String brand){
       int result = carSet.stream().filter(car -> car.getBrand().equalsIgnoreCase(brand)).mapToInt((Car::getValue)).sum();
        System.out.println("Суммарная стоимость для машин модели " + brand + "равна:  " + result);
    }

    public void printAllCar(){
        carSet.stream().peek(System.out::println).collect(Collectors.toList());
    }

    public void printOperation(){
        System.out.println("1-Добавить машину");
        System.out.println("2-Группировать машины по производителю");
        System.out.println("3-Вывести общий список");
        System.out.println("4-Вывести суммарную стоимость по производителю");
        System.out.println("0-Завершить работу");
    }
}


