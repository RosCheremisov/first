package com.levelp.jb2.oop.movies.service;

import com.levelp.jb2.oop.movies.Movie;

public class MovieRunner {

    public static void main(String[] args) {
        MovieWharehouse wharehouse = new MovieWharehouse();

        for (int i = 0; i < 6; ++i) {
            wharehouse.add(createMovie("Titanic" +i));
        }

        wharehouse.delete(3);
        wharehouse.delete(4);

        wharehouse.printAll();
    }

    private static Movie createMovie(String title) {
        Movie movie = new Movie();
        movie.setTitle(title);
        movie.setLogoUrl("some-logo.com.ua");
//        movie.setProducer(new Producer("Kventin", "Tararntino", 77, 85, 100));
//        movie.setActors(new MovieTeamMember[] {
//            new MovieTeamMember("Johny", "Depp", 50, 88),
//            new MovieTeamMember("Bred", "Pitt", 55, 82),
//            new MovieTeamMember("Angelina", "Jolie", 45, 93)
//        });

        return movie;
    }

}
