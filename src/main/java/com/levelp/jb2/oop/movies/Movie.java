package com.levelp.jb2.oop.movies;

public class Movie {

    private Producer producer;

    private String title;
    private String logoUrl;

    private MovieTeamMember[] actors;

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public MovieTeamMember[] getActors() {
        return actors;
    }

    public void setActors(MovieTeamMember[] actors) {
        this.actors = actors;
    }


}
