package com.levelp.jb2.oop.movies.service;

import com.levelp.jb2.oop.movies.Movie;
import java.util.Arrays;

public class MovieWharehouse {

    private static final int STEP = 5;

    private Movie[] movies = new Movie[STEP];
    private int currentPosition = 0;

    public void add(Movie movie) {
        if (currentPosition == movies.length) {
            movies = Arrays.copyOf(movies, movies.length + STEP);
        }
        movies[currentPosition] = movie;
        currentPosition++;
    }

    // [34, 34, null, null]
    // [4,7,4,2,6, null]
    // [1,4,null, 5,3] -> [1,4,5 , null,3] ->  [1,4,5,3, null]
    public void delete(Movie movie) {
        for (int i = 0 ; i < movies.length; ++i) {
            if (movies[i] == movie) {
                if (i + 1 < movies.length || movies[i + 1] == null) {
                    movies[i] = movies[i + 1];
                } else {
                    movies[i + 1] = null;
                    currentPosition = i + 1;
                    break;
                }
            }
        }
    }

    public void delete(int position) {
        for (int i = position ; i < movies.length; ++i) {
            if (i + 1 < movies.length - 1 && movies[i + 1] != null) {
                movies[i] = movies[i + 1];
            } else {
                movies[i ] = null;
                currentPosition = i;
                break;
            }
        }
    }

    public void deleteLast() {
        movies[currentPosition] = null;
        currentPosition--;
    }

    public void printAll() {
        for (Movie movie : movies) {
            if (movie != null) {
                System.out.println(movie);
            }
        }

    }
}
