package com.levelp.jb2.oop.movies;

public class Producer extends MovieTeamMember {

    private int ownOpinion;

    public Producer(String name, String surname, int age, int rating) {
        super(name, surname, age, rating);
    }

    public Producer(String name, String surname, int age, int rating, int ownOpinion) {
        super(name, surname, age, rating);
        this.ownOpinion = ownOpinion;
    }

    public int getOwnOpinion() {
        return ownOpinion;
    }

    public void setOwnOpinion(int ownOpinion) {
        this.ownOpinion = ownOpinion;
    }

    @Override
    public String toString() {
        return "Producer{" + "ownOpinion=" + ownOpinion + ", " + super.toString() + "}";
    }
}
