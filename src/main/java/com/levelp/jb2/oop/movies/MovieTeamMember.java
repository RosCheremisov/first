package com.levelp.jb2.oop.movies;

public class MovieTeamMember {

    private String name;
    private String surname;
    private int age;
    private int rating;

    public MovieTeamMember(String name, String surname, int age, int rating) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "MovieTeamMember{" + "name='" + name + '\'' + ", surname='" + surname + '\'' + ", age=" + age + ", rating=" + rating + '}';
    }
}
