package com.levelp.jb2.oop.geometry.model;

import com.levelp.jb2.util.InputUtil;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Value
@ToString
public class Square extends Shape {
    float sideX;
    @Builder
    public Square(String name, float sideX) {
        super(name);
        this.sideX = sideX;
    }

    public Square(float sideX) {
        this.sideX = sideX;
    }

    @Override
    public float getArea() {
        int pow = 2;
        return (float) Math.pow(getSideX(),pow);
    }

    public static Shape inputData() {
        Square square = Square.builder().sideX(InputUtil.nextFloat("Введите значение стороны: "))
                .name("Square")
                .build();
        System.out.println("Площадь " + square.getName() + " равна: " + square.getArea());
        return square;
    }
}
