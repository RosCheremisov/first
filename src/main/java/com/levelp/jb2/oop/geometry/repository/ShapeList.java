package com.levelp.jb2.oop.geometry.repository;

import com.levelp.jb2.oop.geometry.model.Shape;

import java.util.*;
import java.util.stream.Collectors;

public class ShapeList {
    private final Map<String, List<Shape>> shapeList = new HashMap<>();

    public void add(Shape shape) {
        if (shapeList.containsKey(shape.getName())) {
            shapeList.get(shape.getName()).add(shape);
        } else {
            List<Shape> typeShape = new ArrayList<>();
            typeShape.add(shape);
            shapeList.put(shape.getName(), typeShape);
        }
        System.out.println("Фигура добавлена");
    }


    public void printTotalArea() {
        float result = (float) shapeList.values().stream().flatMap(Collection::stream).mapToDouble(Shape::getArea).sum();
        System.out.println("Общая площадь всех фигур: " + result + " ;");
    }

    public void printTotalAreaByType(String nameShape) {
        float result = (float) shapeList.values().stream()
                                        .flatMap(Collection::stream)
                                        .filter(shape -> shape.getName()
                                        .equalsIgnoreCase(nameShape))
                                        .mapToDouble(Shape::getArea).sum();
        System.out.println("Общая площадь типа " + nameShape + " :" + result + " ;");
    }

    public void printShapeList() {
        shapeList.values().stream().peek(System.out::println).collect(Collectors.toList());
    }

    public void printOperation() {
        System.out.println("1-Создать треугольник");
        System.out.println("2-Создать квадрат");
        System.out.println("3-Создать круг");
        System.out.println("4-Вывести список");
        System.out.println("5-Вывести общую площадь");
        System.out.println("6-Вывести общую площадь типа фигуры");
        System.out.println("0- выход из программы");
    }

}
