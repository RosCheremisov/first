package com.levelp.jb2.oop.geometry.servis;

import com.levelp.jb2.oop.geometry.model.Circle;
import com.levelp.jb2.oop.geometry.model.Square;
import com.levelp.jb2.oop.geometry.model.Triangle;
import com.levelp.jb2.oop.geometry.repository.ShapeList;
import com.levelp.jb2.util.InputUtil;

public class ShapeService {
    ShapeList shapeList = new ShapeList();

    public void run() {
        shapeList.printOperation();
        int starting = InputUtil.nextInt("Выбирете нужную операцию: ");
        while (starting != 0) {
            switch (starting) {
                case 1:
                    shapeList.add(Triangle.inputData());
                    break;
                case 2:
                    shapeList.add(Square.inputData());
                    break;
                case 3:
                    shapeList.add(Circle.inputData());
                    break;
                case 4:
                    shapeList.printShapeList();
                    break;
                case 5:
                    shapeList.printTotalArea();
                    break;
                case 6:
                    String nameShape = InputUtil.nextString("Введите имя фигуры: Triangle, Square, Circle");
                    shapeList.printTotalAreaByType(nameShape);
                    break;
                default:
                    throw new RuntimeException("Ошибка ввода");

            }
            shapeList.printOperation();
            starting = InputUtil.nextInt("Выберите что бы вы хотели выполнить: ");
        }
    }
}
