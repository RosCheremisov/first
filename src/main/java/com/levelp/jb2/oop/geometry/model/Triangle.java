package com.levelp.jb2.oop.geometry.model;

import com.levelp.jb2.util.InputUtil;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Value
@ToString
public class Triangle extends Shape {

    float sideX;
    float sideY;
    float sideZ;

    @Builder
    public Triangle(String name, float sideX, float sideY, float sideZ) {
        super(name);
        this.sideX = sideX;
        this.sideY = sideY;
        this.sideZ = sideZ;
    }

    @Override
    public float getArea() {
        float p = 0.5f * (getSideX() + getSideY() + getSideZ());
        return (float) Math.sqrt(p * (p - getSideX()) * (p - getSideY()) * (p - getSideZ()));
    }

    public static Triangle inputData() {
        Triangle triangle = Triangle.builder().sideX(InputUtil.nextFloat("Введите значения стороны X"))
                .name("Triangle")
                .sideY(InputUtil.nextFloat("Введите значения стороны Y"))
                .sideZ(InputUtil.nextFloat("Введите значения стороны Z"))
                .build();
        System.out.println("Площадь " + triangle.getName() + " равна: " + triangle.getArea());
        return triangle;
    }
}
