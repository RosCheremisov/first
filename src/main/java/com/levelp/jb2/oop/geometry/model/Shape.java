package com.levelp.jb2.oop.geometry.model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class Shape {
    private String name;

    public abstract float getArea();
}
