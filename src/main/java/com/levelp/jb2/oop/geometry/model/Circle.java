package com.levelp.jb2.oop.geometry.model;

import com.levelp.jb2.util.InputUtil;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Value
@ToString
public class Circle extends Shape {
    private static final float PI = 3.14f;
    float radius;

    @Builder
    public Circle(String name, float radius) {
        super(name);
        this.radius = radius;
    }

    @Override
    public float getArea() {
        int pow = 2;
        return (float) (PI * Math.pow(getRadius(), pow));
    }

    public static Circle inputData() {
        Circle circle = Circle.builder().radius(InputUtil.nextFloat("Введите радиус"))
                .name("Circle")
                .build();
        System.out.println("Площадь " + circle.getName() + " равна: " + circle.getArea());
        return circle;
    }
}
