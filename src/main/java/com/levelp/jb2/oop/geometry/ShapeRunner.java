package com.levelp.jb2.oop.geometry;

import com.levelp.jb2.oop.geometry.servis.ShapeService;

/**
 * Есть фигуры - треугольник, квадрат, круг -нужно для каждого уметь вычислить площадь фигуры. Каждый хранит свои данные.
 * Нужно создать Window - будет хранить массив фигур. Нужно уметь вывести общую площать всех фигур, и общую площать для каждого типа фигур
 */
public class ShapeRunner {
    public static void main(String[] args) {
        ShapeService shapeService = new ShapeService();
        shapeService.run();
    }
}
