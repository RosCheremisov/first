package com.levelp.jb2.oop.tv;

public class QLEDTV extends AbstractTV {

    public QLEDTV(int inchSize) {
        super(inchSize);
    }

    @Override
    String getType() {
        return "QLED";
    }
}
