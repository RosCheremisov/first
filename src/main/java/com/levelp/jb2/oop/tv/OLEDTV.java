package com.levelp.jb2.oop.tv;

public class OLEDTV extends AbstractTV {

    public OLEDTV(int inchSize) {
        super(inchSize);
    }

    @Override
    String getType() {
        return "OLED";
    }
}
