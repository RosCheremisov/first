package com.levelp.jb2.oop.tv;

/**
 * открыты для расширение - закрыты для изменения
 */

/**
 * OldSchoolBlackWhiteTv, LCDTv, QLEDTv, OLEDTv
 */
public abstract class AbstractTV {

    private final int inchSize;

    public AbstractTV(int inchSize) {
        this.inchSize = inchSize;
    }

    public int getInchSize() {
        return inchSize;
    }

    void draw() {
        System.out.println("This is " + getType() + " TV, size: " + getInchSize() + " inches");
    }

    abstract String getType();

}
