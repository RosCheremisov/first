package com.levelp.jb2.oop.tv;

public class OldSchoolBlackWhiteTV extends AbstractTV {

    public OldSchoolBlackWhiteTV(int inchSize) {
        super(inchSize);
    }



    @Override
    String getType() {
        return "BlackWhite";
    }
}
