package com.levelp.jb2.oop.tv;

public class BuyTvRunner {

    public static void main(String[] args) {
        AbstractTV oldTv = new OldSchoolBlackWhiteTV(24);
        oldTv.draw();
        new OLEDTV(30).draw();
    }

}
