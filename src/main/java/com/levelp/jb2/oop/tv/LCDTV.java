package com.levelp.jb2.oop.tv;

public class LCDTV extends AbstractTV {

    public LCDTV(int inchSize) {
        super(inchSize);
    }

    @Override
    String getType() {
        return "LCD";
    }
}
