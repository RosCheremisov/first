package com.levelp.jb2.oop.shape;

public class Triangle extends Shape {

    @Override
    String getShapeName() {
        return "Triangle";
    }
}
