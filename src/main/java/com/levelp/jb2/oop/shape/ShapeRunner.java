package com.levelp.jb2.oop.shape;

public class ShapeRunner {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle();
        rectangle.draw();
        Triangle triangle = new Triangle();
        triangle.draw();
        Circle circle = new Circle();
        circle.draw();
    }

}
