package com.levelp.jb2.oop.shape;

/**Создать класс фигура Shape - с методом draw , создать наследников Rectangle, Triangle, Circle.
 *  В методе  draw вывести на консоль название фигуры - создать разные экземпляры классов и вызвать метод draw
 *
 */

public abstract class Shape {

    void draw(){
        System.out.println("Это фигура: " + getShapeName());
    }

    abstract String getShapeName();

}
