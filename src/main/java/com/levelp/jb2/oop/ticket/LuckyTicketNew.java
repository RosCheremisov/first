package com.levelp.jb2.oop.ticket;

import java.util.*;

public class LuckyTicketNew {
    LinkedList<Integer> luckyTicketList = new LinkedList<Integer>();
    public static void main(String[] args) {
        LuckyTicketNew aNew = new LuckyTicketNew();
        System.out.println("Количество счастливых билетов: " + aNew.getLuckyTicket());
        System.out.println("Первый: " + aNew.luckyTicketList.getFirst() + " Последний: " + aNew.luckyTicketList.getLast());
    }
    public Integer getLuckyTicket() {
        int valueLuckyTicket = 0;
        for (int i = 1000; i < 1000000; i++) {
            if (i / 100000 + i / 10000 % 10 + i / 1000 % 10 == i / 100 % 10 + i / 10 % 10 + i % 10) {
                valueLuckyTicket++;
                luckyTicketList.add(i);
            }
        }
        return valueLuckyTicket;
    }
}
