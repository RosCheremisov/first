package com.levelp.jb2.oop.members;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class Aspirant extends Student {

    private String study;

    public Aspirant(String firstName, String lastName, String group, double averageMark, String study) {
        super(firstName, lastName, group, averageMark);
        this.study = study;
    }

    public int getScholarship() {
        this.scholarship = averageMark <= 5 ? 180 : 200;
        return scholarship;
    }
}

