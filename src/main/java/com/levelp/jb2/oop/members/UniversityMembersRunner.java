package com.levelp.jb2.oop.members;

import java.util.Arrays;

/**
 *
 */
public class UniversityMembersRunner {
    public static void main(String[] args) {
        Student[] list = new Student[4];
        list[0] = new Student("Олег", "Olegov", "Programing ING", 4.5);
        list[1] = new Aspirant("Cергей", "Sergeev", "Functional Computing", 5.2, "Covid19 - get out");
        list[2] = new Aspirant("olol", "Sergeev", "Functional Computing", 3.2, "Covid19 - get out");
        list[3] = new Aspirant("Igogog", "Sergeev", "Functional Computing", 4.2, "Covid19 - get out");

        System.out.println(Arrays.toString(list));
    }
}
