package com.levelp.jb2.oop.members;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Student {

    protected String firstName;
    protected String lastName;
    protected String group;

    protected double averageMark;
    protected int scholarship;

    public Student(String firstName, String lastName, String group, double averageMark) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.group = group;
        this.averageMark = averageMark;
        this.scholarship = getScholarship();
    }

    public int getScholarship() {
        this.scholarship = averageMark <= 5 ? 80 : 100;
        return scholarship;
    }
}
