package com.levelp.jb2.oop.interfaces;

import java.util.Scanner;

public class PaymentSystemRunner {

//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        String type = scanner.next();
//        int amount = scanner.nextInt();
//
//        if (!"u".equalsIgnoreCase(type)) {
//            new PaymentSystemRunner().processPayment(new AlphaPaymentServiceImpl(), amount);
//        } else {
//            new PaymentSystemRunner().processPayment(new UkrsibPaymentServiceImpl(), amount);
//        }
//    }

    void processPayment(PaymentService service, int amount) {
        service.charge(amount);
    }
}
