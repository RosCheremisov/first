package com.levelp.jb2.oop.interfaces;

public class AlphaPaymentServiceImpl implements PaymentService, LoggingService {

    @Override
    public void charge(int amount) {
        System.out.println("another logic" + amount*2);
    }

    @Override
    public void logInfo(String data) {

    }
}
