package com.levelp.jb2.oop.interfaces;

public interface PaymentService {

    void charge(int amount);

}
