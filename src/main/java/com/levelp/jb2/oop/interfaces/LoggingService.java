package com.levelp.jb2.oop.interfaces;

public interface LoggingService {

    void logInfo(String data);

}
