package com.levelp.jb2.oop.factory.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum EmployeeTitle {
    EMPLOYEE(1),
    DIRECTOR(2),
    MANAGER(3);

    public int inputType;

    public static EmployeeTitle selectTitle(int inputType) {
        for (EmployeeTitle value : values()) {
            if (value.inputType == inputType) {
                return value;
            }
        }
        throw new RuntimeException("Не верный ввод");
    }
}