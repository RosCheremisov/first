package com.levelp.jb2.oop.factory.model;

import lombok.*;

import java.util.Arrays;

import static com.levelp.jb2.oop.factory.model.EmployeeTitle.selectTitle;
import static com.levelp.jb2.util.InputUtil.nextInt;
import static com.levelp.jb2.util.InputUtil.nextString;

@Data
@EqualsAndHashCode(of = {"firstName", "lastName"})
@RequiredArgsConstructor
@NoArgsConstructor
public class Employee implements Comparable<Employee> {
    @NonNull
    private String firstName;
    @NonNull
    private String lastName;
    private EmployeeTitle title;
    private int salary;

    public int compareTo(Employee employee) {
        if (this.salary == employee.salary) {
            return 0;
        } else if (this.salary > employee.salary) {
            return 1;
        } else {
            return -1;
        }
    }

    public static Employee createEmployeeData() {
        Employee employee = new Employee();
        employee.setFirstName(nextString("Введите имя работника: "));
        employee.setLastName(nextString("Введите фамилию работника: "));
        System.out.println(Arrays.toString(EmployeeTitle.values()));
        employee.setTitle(selectTitle(nextInt("Введите должность работника: ")));
        employee.setSalary(nextInt("Введите зарпалату работника: "));
        return employee;
    }
}
