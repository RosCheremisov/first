package com.levelp.jb2.oop.factory.service;

import com.levelp.jb2.oop.factory.model.Employee;
import com.levelp.jb2.oop.factory.model.EmployeeTitle;
import com.levelp.jb2.oop.factory.repository.EmployeeDataset;
import com.levelp.jb2.util.InputUtil;

import java.util.Arrays;


public class HiringProcess {
    private  EmployeeDataset dataset = new EmployeeDataset();

    public void run() {
        dataset.printOperation();
        int starting = InputUtil.nextInt("Выбирте что бы вы хотели выполнить: ");
        while (starting != 0) {
            switch (starting) {
                case 1:
                    dataset.add(Employee.createEmployeeData());
                    break;
                case 2:
                    String firstName = InputUtil.nextString("Введите имя работника: ");
                    String lastName = InputUtil.nextString("Введите фамилию работника: ");
                    dataset.fireEmployee(firstName, lastName);
                    break;
                case 3:
                    dataset.printAverageSalary();
                    break;
                case 4:
                    System.out.println(Arrays.toString(EmployeeTitle.values()));
                    int select = InputUtil.nextInt("Выберите должность: ");
                    dataset.printAverageSalaryByTitle(EmployeeTitle.selectTitle(select));
                    break;
                case 5:
                    dataset.printSalaryMax();
                    break;
                case 6:
                    dataset.printSalaryMin();
                    break;
                case 7:
                    dataset.print();
                    break;
                default:
                    throw new RuntimeException("Не верный выбор");
            }
            dataset.printOperation();
            starting = InputUtil.nextInt("Выберите что бы вы хотели выполнить: ");
        }
    }
}
