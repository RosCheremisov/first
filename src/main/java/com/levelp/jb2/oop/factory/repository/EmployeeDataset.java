package com.levelp.jb2.oop.factory.repository;

import com.levelp.jb2.oop.factory.model.Employee;
import com.levelp.jb2.oop.factory.model.EmployeeTitle;

import java.util.*;
import java.util.stream.Collectors;

public class EmployeeDataset {

    private final Map<EmployeeTitle, NavigableSet<Employee>> employeeMap = new HashMap<>();

    public void add(Employee worker) {
        if (employeeMap.containsKey(worker.getTitle())) {
            employeeMap.get(worker.getTitle()).add(worker);
        } else {
            NavigableSet<Employee> EmployeeList = new TreeSet<>(Employee::compareTo);
            EmployeeList.add(worker);
            employeeMap.put(worker.getTitle(), EmployeeList);
        }
        System.out.println("Рабочий: " + worker.getLastName() + " " + worker.getFirstName() + " нанят");
    }

    public boolean fireEmployee(String firstName, String lastName) {
        for (Set<Employee> currentList : employeeMap.values()) {
            Employee employeeToFire = new Employee(firstName, lastName);
            if (currentList.contains(employeeToFire)) {
                currentList.remove(employeeToFire);
                System.out.println("Successfully fired: " + firstName + " " + lastName);
                return true;
            }
        }
        System.out.println("Couldn't find employee in our dataset - " + firstName + " " + lastName);
        return false;
    }

    public void print() {
        for (Set<Employee> currentList : employeeMap.values()) {
            System.out.println(currentList);
        }
    }

    public void printAverageSalary() {
        double average = employeeMap.values().stream().flatMap(Collection::stream)
                .collect(Collectors.averagingInt(Employee::getSalary));
        System.out.println("Средняя зарпалата по фирме: " + average + ";");
    }

    public void printAverageSalaryByTitle(EmployeeTitle title) {
        double average = employeeMap.values().stream().flatMap(Collection::stream)
                .filter(employee -> employee.getTitle().equals(title))
                .collect(Collectors.averagingInt((Employee::getSalary)));
        System.out.println("Средня зарпалата по должности: " + title + " составляет: " + average);
    }

    public void printSalaryMax() {
        employeeMap.values().stream().flatMap(Collection::stream)
                .max(Employee::compareTo).ifPresent(employee -> System.out.println("Максимальная зарпалата на фирме " + employee + ";"));
    }

    public void printSalaryMin() {
        employeeMap.values().stream().flatMap(Collection::stream)
                .min(Employee::compareTo)
                .ifPresent(employee -> System.out.println("минимальная зарплата на фирме " + employee + ";"));
    }

    public void printOperation() {
        System.out.println("1 - Принять на работу.");
        System.out.println("2 - Уволить с работы.");
        System.out.println("3 - Среднея зарпалата по фирме.");
        System.out.println("4 - Среднея зарпалата по должности.");
        System.out.println("5 - Максимальная зарплата.");
        System.out.println("6 - Минимальная зарплата");
        System.out.println("7 - Вывести список.");
        System.out.println("0 - Завершить работу ");
    }
}




