package com.levelp.jb2.oop.factory;

import com.levelp.jb2.oop.factory.service.HiringProcess;

import java.util.Queue;

/**
 * Создать программу которая будет хранить состав работников завода. Работник - имя, фамилия, должность, оклад.
 * Функции - нанять, уволить, вывести список.
 * Иметь возможность посчитать среднюю зп по компании, средняя по должности, самая высокая и самая низкая зп. Должности - EMPLOYEE, DIRECTOR, MANAGER
 */

public class FactoryRunner {
    public static void main(String[] args) {
        HiringProcess hiringProcess = new HiringProcess();
        hiringProcess.run();
    }

}
