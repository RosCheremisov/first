package com.levelp.jb2.oop.calculator;

import static com.levelp.jb2.util.InputUtil.nextString;

public class CalculatorValidator {

    private static final String[] OPERATIONS = {"+", "-", "*", "/", "^"};

    public static String getValidatedInput(String description) {
        String input = nextString(description);
        for (String operation : OPERATIONS) {
            if (input.equalsIgnoreCase(operation)) {
                return input;
            }
        }
        System.out.println("Вы ввели неправильную строку, попробуйте еще");
        return getValidatedInput(description);

    }

}
