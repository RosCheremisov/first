package com.levelp.jb2.oop.calculator;

public class Calculator {

    public static long calculate(int a, int b, String operation) {
        switch (operation) {
            case ("+"):
                return sum(a, b);
            case ("-"):
                return minus(a, b);
            case ("*"):
                return multiply(a, b);
            case ("/"):
                return divide(a, b);
            case ("^"):
                return multiply2(a);
            default:
                throw new RuntimeException("Некорректный ввод символа");
        }
    }

    private static int sum(int a, int b) {
        return a + b;
    }

    private static long multiply(int a, int b) {
        return (long) a * (long) b;
    }

    private static int minus(int a, int b) {
        return a - b;
    }

    private static int divide(int a, int b) {
        try {
            return a / b;
        } catch (ArithmeticException e) {
            System.out.println("делить на 0 запрещено");
            throw new DivideOnNullException();
        }
    }

    private static int multiply2(int a) throws ArithmeticException {
        return a * a;
    }
}
