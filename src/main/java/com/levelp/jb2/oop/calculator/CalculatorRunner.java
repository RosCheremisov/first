package com.levelp.jb2.oop.calculator;

public class CalculatorRunner {

    public static void main(String[] args) {
        CalculatorService calculator = new CalculatorService();
        calculator.process();
    }

}
