package com.levelp.jb2.oop.calculator;

import static com.levelp.jb2.oop.calculator.Calculator.calculate;
import static com.levelp.jb2.oop.calculator.CalculatorValidator.getValidatedInput;
import static com.levelp.jb2.util.InputUtil.nextInt;
import static com.levelp.jb2.util.InputUtil.nextString;

public class CalculatorService {

    public void process() {
        int number2 = 0;
        do {
            System.out.println("Начинаем работу с калькулятором");
            int number1 = nextInt();
            String operation = getValidatedInput("Введите символ + - / * ^ для определения операции");
            if (!operation.equalsIgnoreCase("^")) {
                number2 = nextInt();
            }
            try {
                System.out.println("Result is - " + calculate(number1, number2, operation));
            } catch (ArithmeticException e) {
                System.out.println("Got error " + e.getMessage());
            }
        } while (!"n".equalsIgnoreCase(nextString("Для выхода введите n - иначе любой символ")));
    }

}

