package com.levelp.jb2.oop.calculator;

public class DivideOnNullException extends RuntimeException {

    public DivideOnNullException() {
        super("Never divide on 0");
    }
}
