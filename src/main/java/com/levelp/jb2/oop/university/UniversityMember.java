package com.levelp.jb2.oop.university;

public class UniversityMember {

    private String name;
    private String surname;
    private UniversityMemberType memberType;

    public UniversityMember(String name, String surname, UniversityMemberType memberType) {
        this.name = name;
        this.surname = surname;
        this.memberType = memberType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public UniversityMemberType getMemberType() {
        return memberType;
    }

    public void setMemberType(UniversityMemberType memberType) {
        this.memberType = memberType;
    }

    @Override
    public String toString() {
        return "UniversityMember{" + "name='" + name + '\'' + ", surname='" + surname + '\'' + ", memberType=" + memberType + '}';
    }
}
