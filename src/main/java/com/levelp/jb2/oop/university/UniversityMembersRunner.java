package com.levelp.jb2.oop.university;

import java.util.Random;

public class UniversityMembersRunner {
    public static void main(String[] args) {
        UniversityMembersRunner membersRunner = new UniversityMembersRunner();
        UniversityMember[] members = membersRunner.generateArray();

        for (UniversityMember member : members) {
            if (member.getMemberType() == UniversityMemberType.TEACHER) {
                System.out.println(member);
            }
        }
    }

    private UniversityMember[] generateArray() {
        UniversityMember[] users = new UniversityMember[10];
        users[0] = generateMember("Ivan", "Ivanov");
        users[1] = generateMember("Petr", "Petrov");
        users[2] = generateMember("Sidor", "Sidorov");
        users[3] = generateMember("Oleg", "Olegov");
        users[4] = generateMember("Vlad", "Vladov");
        users[5] = generateMember("Ahmed", "Ahmedov");
        users[6] = generateMember("Anton", "Antonov");
        users[7] = generateMember("Olg", "Olgov");
        users[8] = generateMember("Carl", "Carlov");
        users[9] = generateMember("Olga", "Olgova");
        return users;
    }

    private UniversityMember generateMember(String name, String surname) {
        int nextPosition = new Random().nextInt(4);
        UniversityMemberType type = UniversityMemberType.values()[nextPosition];
        return new UniversityMember(name, surname, type);
    }
}
