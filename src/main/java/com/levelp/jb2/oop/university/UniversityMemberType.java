package com.levelp.jb2.oop.university;

/**
 *
 */

// ordinal - номер позиции в котором определен ENUM значени
public enum UniversityMemberType {

    STUDENT("Students only", 1), // ordinal = 0
    ASPIRANT("Aspirants only", 2), // ordinal = 1
    TEACHER("Teacher", 3),
    DIRECTOR("Head officer", 4);

    private int order;
    private String description;

    UniversityMemberType(String description, int order) {
        this.description = description;
        this.order = order;
    }

    public String getDescription() {
        return description;
    }

    public int getOrder() {
        return order;
    }
}