package com.levelp.jb2.oop.converter.service;

import com.levelp.jb2.oop.converter.model.CurrencyItem;

public class EurUahCurrencyConverterImpl implements CurrencyConverter {

    private final float multiplier = 30f;

    @Override
    public CurrencyItem convertTo(CurrencyItem initial) {
        if (initial.getName().equalsIgnoreCase(EURO_DEFAULT_SIGN)) {
            CurrencyItem currencyItem = new CurrencyItem(multiplier * initial.getAmount(), UAH_DEFAULT_SIGN);
            System.out.println("Initial : " + initial + " - become : " + currencyItem);
            return currencyItem;
        }
        throw new RuntimeException("Invalid input currency.Should is EURO");
    }

    @Override
    public CurrencyItem convertFrom(CurrencyItem initial) {
        if (initial.getName().equalsIgnoreCase(UAH_DEFAULT_SIGN)) {
            CurrencyItem currencyItem = new CurrencyItem(initial.getAmount() / multiplier, EURO_DEFAULT_SIGN);
            System.out.println("Initial : " + initial + " - become : " + currencyItem);
            return currencyItem;
        }
        throw new RuntimeException("Invalid input currency.Should is HRYVNAS");
    }
}
