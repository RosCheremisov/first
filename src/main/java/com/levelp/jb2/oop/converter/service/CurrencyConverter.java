package com.levelp.jb2.oop.converter.service;

import com.levelp.jb2.oop.converter.model.CurrencyItem;

public interface CurrencyConverter {

    String EURO_DEFAULT_SIGN = "EUR";
    String USD_DEFAULT_SIGN = "USD";
    String UAH_DEFAULT_SIGN = "UAH";

    CurrencyItem convertTo(CurrencyItem initial);
    CurrencyItem convertFrom(CurrencyItem initial);
}
