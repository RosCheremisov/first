package com.levelp.jb2.oop.converter.model;

public class Deposit {

    private CurrencyItem depositAccount;

    public void refillDeposit() {
        getDepositAccount().setAmount(getDepositAccount().getAmount() + depositAccount.getAmount());
        getDepositAccount().setName(depositAccount.getName());
        System.out.println("Deposit " + " " + getDepositAccount().getName() + " : " + getDepositAccount().getAmount());
    }

    public void withdrawDeposit(float a) {
        getDepositAccount().setAmount(getDepositAccount().getAmount() - a);
        System.out.println("Deposit " + " " + getDepositAccount().getName() + " : " + getDepositAccount().getAmount());
    }

    public CurrencyItem getDepositAccount() {
        return depositAccount;
    }

    public void setDepositAccount(CurrencyItem depositAccount) {
        this.depositAccount = depositAccount;
    }
}
