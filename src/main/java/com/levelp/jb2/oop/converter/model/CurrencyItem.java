package com.levelp.jb2.oop.converter.model;

// 5 eur, 10 usd
public class CurrencyItem {

    private float amount;
    private String name;

    public CurrencyItem(float amount, String name) {
        this.amount = amount;
        this.name = name;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CurrencyItem{" +
                "amount=" + amount +
                ", currencyName='" + name + '\'' +
                '}';
    }
}
