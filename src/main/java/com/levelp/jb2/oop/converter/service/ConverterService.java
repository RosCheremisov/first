package com.levelp.jb2.oop.converter.service;

import com.levelp.jb2.oop.converter.model.CurrencyItem;

public class ConverterService {

    public static CurrencyItem convert(String currencyFrom, String currencyTo, float amount) {
        switch (currencyFrom) {
            case CurrencyConverter.USD_DEFAULT_SIGN: {
                switch (currencyTo) {
                    case CurrencyConverter.UAH_DEFAULT_SIGN: {
                        return new UsdUahCurrencyConverterImpl().convertTo(new CurrencyItem(amount, currencyFrom));
                    }
                    case CurrencyConverter.EURO_DEFAULT_SIGN: {
                        return new UsdEurCurrencyConverterImpl().convertFrom(new CurrencyItem(amount, currencyFrom));
                    }
                }
            }
            case CurrencyConverter.EURO_DEFAULT_SIGN: {
                switch (currencyTo) {
                    case CurrencyConverter.UAH_DEFAULT_SIGN: {
                        return new EurUahCurrencyConverterImpl().convertTo(new CurrencyItem(amount, currencyFrom));
                    }
                    case CurrencyConverter.USD_DEFAULT_SIGN: {
                        return new UsdEurCurrencyConverterImpl().convertTo(new CurrencyItem(amount, currencyFrom));
                    }
                }
            }
            case CurrencyConverter.UAH_DEFAULT_SIGN: {
                switch (currencyTo) {
                    case CurrencyConverter.USD_DEFAULT_SIGN: {
                        return new UsdUahCurrencyConverterImpl().convertFrom(new CurrencyItem(amount, currencyFrom));
                    }
                    case CurrencyConverter.EURO_DEFAULT_SIGN: {
                        return new EurUahCurrencyConverterImpl().convertFrom(new CurrencyItem(amount, currencyFrom));
                    }
                }
            }
            default: {
                throw new RuntimeException("Not existed case for now");
            }
        }
    }
}
