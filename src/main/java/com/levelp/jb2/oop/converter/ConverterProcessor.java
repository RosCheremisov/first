package com.levelp.jb2.oop.converter;

import static com.levelp.jb2.oop.converter.service.ConverterService.convert;
import static com.levelp.jb2.util.InputUtil.nextString;

import com.levelp.jb2.oop.converter.model.CurrencyItem;
import com.levelp.jb2.oop.converter.model.Deposit;
import com.levelp.jb2.util.InputUtil;

public class ConverterProcessor {

    private final Deposit deposit = new Deposit();

    public void run() {
        CurrencyItem initialValue = getInitialValues();
        do {
            String selectedCurrency = nextString("Введите валюту ");
            CurrencyItem depositItemMoney = convert(deposit.getDepositAccount().getName(), selectedCurrency, initialValue.getAmount());
            deposit.setDepositAccount(depositItemMoney);

            if (initialValue.getName().equalsIgnoreCase("UAH")) {
                processUahDeposit();
            }
        } while (!"n".equalsIgnoreCase(nextString("Для завершения работы введите: n, Для продолжения любой символ")));
    }

    private void processUahDeposit() {
        if (checkCondition("Хотите положить деньги на счет? y или n")) {
            deposit.refillDeposit();
        } else if (checkCondition("Хотели бы снять депозит? y или n")) {
            float selectedWithDrawDeposit = InputUtil.nextFloat("Какую сумму снять? :");
            deposit.withdrawDeposit(selectedWithDrawDeposit);
        }
    }

    private boolean checkCondition(String description) {
        String operation = nextString(description);
        return operation.equalsIgnoreCase("y");
    }

    private CurrencyItem getInitialValues() {
        float initialMoney = InputUtil.nextFloat("Введите начальную сумму денег");
        String initialCurrency = nextString("Введите начальную валюту");
        return new CurrencyItem(initialMoney, initialCurrency);
    }
}

