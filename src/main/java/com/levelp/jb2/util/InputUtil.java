package com.levelp.jb2.util;

import java.util.InputMismatchException;
import java.util.Scanner;

public class InputUtil {

    private static final String DEFAULT_STRING_INVITATION = "Введите слово: ";
    private static final String DEFAULT_INT_INVITATION = "Введите число: ";

    public static int nextInt() {
        return nextInt(DEFAULT_INT_INVITATION);
    }

    public static int nextInt(String description) {
        try {
            System.out.println(description);
            Scanner scanner = new Scanner(System.in);
            return scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Вы ввели неправильный символ, пожалуйста попробуйте еще - принимается только число");
            return nextInt();
        }
    }

    public static String nextString() {
        return nextString(DEFAULT_STRING_INVITATION);
    }

    public static String nextString(String description) {
        System.out.println(description);
        Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }

    public static float nextFloat() {
        return nextFloat(DEFAULT_INT_INVITATION);
    }

    public static float nextFloat(String description) {
        try {
            System.out.println(description);
            Scanner scanner = new Scanner(System.in);
            return scanner.nextFloat();
        } catch (InputMismatchException e) {
            System.out.println("Вы ввели неправильный символ, пожалуйста попробуйте еще - принимается только число");
            return nextFloat();
        }
    }

    public static double nextDouble() {
        return nextDouble(DEFAULT_INT_INVITATION);
    }

    public static double nextDouble(String description) {
        try {
            System.out.println(description);
            Scanner scanner = new Scanner(System.in);
            return scanner.nextDouble();
        } catch (InputMismatchException e) {
            System.out.println("Вы ввели неправильный символ, пожалуйста попробуйте еще - принимается только число");
            return nextDouble();
        }
    }
}
