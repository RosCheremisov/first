package com.levelp.jb2.util;

import java.util.Scanner;

public class ArrayUtil {

    public static String[] getStringArray(int size) {
        Scanner scanner = new Scanner(System.in);
        String[] strArray = new String[size];
        for (int i = 0; i < size; ++i) {
            strArray[i] = scanner.next();
        }
        return strArray;
    }
}
