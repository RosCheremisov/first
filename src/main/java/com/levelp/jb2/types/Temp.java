package com.levelp.jb2.types;

public class Temp {
    public static void main(String[] args) {
        int temp = 0;
        System.out.println("Step1 = " + temp);
        temp = 2;
        System.out.println("Step2 = " + temp);
        temp = temp + 1;
        System.out.println("Step3 = " + temp);
        temp++;
        System.out.println("Step4 = " + temp);
        temp = temp - 4;
        System.out.println("Step5 = " + temp);
        for (int i = 0; i < 5; i++) {
            temp--;
            System.out.println("Step6 = " + temp);
            int test = 0;
        }
        String str1 = "Ivan";
        String str2 = "Petr";
        String str3 = str1 + "+" + str2;
        System.out.println(str1);
        System.out.println(str2);
        System.out.println(str3);
        boolean isAlive = false;
        System.out.println(isAlive + " Живой ");
        isAlive = true;
        System.out.println(isAlive + " Не живой");
        isAlive = temp == -4;
        System.out.println(isAlive + "   ");


    }
}
