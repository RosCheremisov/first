package com.levelp.jb2.types;

/**
 * 1. На входе строка - нужно заменить все символы 'd' на "new" + если строка короче10 символов, то вывести эту строку + ее первую половину
 */

import com.levelp.jb2.util.InputUtil;

public class StringChange {
    public static void main(String[] args) {
        String str = InputUtil.nextString();
        getReplaceSymbol(str);

        for (int i = 0; i < str.length(); i++) {
            getHalfWord(getReplaceSymbol(str));
        }
        printWord(getReplaceSymbol(str));
        printWord(getHalfWord(str));
    }


    public static String getReplaceSymbol(String str) {
        if (str.contains("d")) {
            str = str.replace("d", "new");
        }
        return str;
    }

    public static String getHalfWord(String str) {
        if (str.length() < 10) {
            str = str + str.substring(0, str.length() / 2);
        }
        return str;
    }

    public static void printWord(String str) {
        System.out.println(str);
        System.out.println();
    }
}


