package com.levelp.jb2.loops;

import java.util.Arrays;

public class ArraySortBub {

    public static void main(String[] args) {
        int[] array = {1, 5, 3, 2, 6, 3, 5};
        boolean isSort = false;
        int buf;
        while (!isSort) {
            isSort = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    buf = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = buf;
                    isSort = false;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }
}
