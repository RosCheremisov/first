package com.levelp.jb2.collection.linkedlist;

public class List {
    private Node head;
    private Node tail;

    public void addStart(int data) {
        Node a = new Node();
        a.data = data;
        if (head == null) {
            head = a;
            tail = a;
        } else {
            a.next = head;
            head = head.prev;
            head = a;
        }
    }

    public void addEnd(int data) {
        Node a = new Node();
        a.data = data;
        if (tail == null) {
            head = a;
            tail = a;
        } else {
            tail.next = a;
            tail.prev = tail;
            tail = a;
        }
    }

    public boolean hasNext(Node listElements) {
        return listElements.next != null;
    }

    public void printList() {
        Node t = head;
        while (hasNext(t)) {
            System.out.print(" " + t.data);
            t = t.next;
        }
    }
}
