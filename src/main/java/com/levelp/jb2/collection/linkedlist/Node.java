package com.levelp.jb2.collection.linkedlist;

public class Node {
    public int data;
    public Node next;
    public Node prev;
}
