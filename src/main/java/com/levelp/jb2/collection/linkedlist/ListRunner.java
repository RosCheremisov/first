package com.levelp.jb2.collection.linkedlist;

public class ListRunner {
    public static void main(String[] args) {
        List firstList = new List();
        firstList.addStart(5);
        firstList.addStart(10);
        firstList.addEnd(3);
        firstList.addStart(8);
        firstList.addEnd(7);
        firstList.addStart(5);
        firstList.addEnd(6);
        firstList.printList();
    }
}
