package com.levelp.jb2.cache.LRU;

public class Runner {
    public static void main(String[] args) {
        LruCache<Integer,String> cache = new LruCache<>(3);
        cache.put(1,"a");
        cache.put(2,"b");
        cache.put(3,"c");
        cache.get(2);
        cache.put(0,"d");
        System.out.println(cache);
    }
}
