package com.levelp.jb2.txt.writer;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Создать новый текстовый файл и записать туда Меня зовуцт Ростик
 */

@Slf4j
public class TxtWriter {
    public static void main(String[] args) {
        String name = "Меня зовут Ростик";
        writerTxt(name);
    }

    public static void writerTxt(String s) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File("./src/main/resources/name.txt")))) {
            writer.write(s);
        } catch (IOException e) {
            log.error("Ошибка", e);
        }
    }
}

