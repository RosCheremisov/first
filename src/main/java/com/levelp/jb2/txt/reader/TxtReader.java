package com.levelp.jb2.txt.reader;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Считать данные из файла все логи с типом ERROR и вывести в консоль
 */

@Slf4j
public class TxtReader {
    public static void main(String[] args) {
        txtReader();

    }

    public static void txtReader() {
        Path path = Paths.get("./src/main/resources/massagek.txt");
        try (Stream<String> stream = Files.lines(path)) {
            stream.filter(str -> str.contains("ERROR")).forEach(System.out::println);
        } catch (IOException e) {
            log.error("Ошибка",e);
        }
    }
}
