package com.levelp.jb2;

import java.util.Scanner;

public class Palindrome {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите слово");
        String word = scanner.next();
        StringBuilder buffer = new StringBuilder(word);
        buffer.reverse();
        String rev = buffer.toString();
        if (word.equals(rev)) {
            System.out.println("Слово полиндром " + word + " " + rev);
        } else {
            System.out.println("Слово не полиндром: " + word + " " + rev);
        }
    }
}
