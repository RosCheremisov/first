package com.levelp.jb2.map;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/***
 * на входе - номер лотерейного билета (строка), выигрышные серия номеров (fgdsdfgfsdg -> [1,4,5,6,7,12], gfhfgh -> [2,5,6,98])
 * на выходе вывести номер победившего лотерейного билета
 */
public class MapStorage {

    private final Map<String, List<Integer>> values = new HashMap<>();

    public void add(String key, Integer value) {
        if (values.containsKey(key)) {
            values.get(key).add(value);
        } else {
            List<Integer> results = new ArrayList<>();
            results.add(value);
            values.put(key, results);
        }
    }

    public void print() {
        for (Entry<String, List<Integer>> entry : values.entrySet()) {
            System.out.println("For key : " + entry.getKey() + " values - " + entry.getValue());
        }
    }

    public String getWinnersTicketNumber(List<Integer> numbers) {
        for (Entry<String, List<Integer>> entry : values.entrySet()) {
            if (entry.getValue().containsAll(numbers)) {
                return entry.getKey();
            }
        }
        return null;
    }
}
