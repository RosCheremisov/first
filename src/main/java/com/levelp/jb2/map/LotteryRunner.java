package com.levelp.jb2.map;

import static java.util.Arrays.asList;

import java.util.List;
import java.util.stream.Stream;

public class LotteryRunner {

    private final MapStorage mapStorage = new MapStorage();

    public static void main(String[] args) {
        LotteryRunner runner = new LotteryRunner();
        runner.fillTickets();
        String winner = runner.mapStorage.getWinnersTicketNumber(asList(1, 3, 4, 6, 122));
        System.out.println("Our winner is - " + winner);
    }

    public void fillTickets() {
        addTicket("ticket1", asList(1, 3, 4, 6, 12));
        addTicket("ticketVasa", asList(1, 31, 41, 61, 12));
        addTicket("ololo", asList(1, 34,4,  6, 12));
        addTicket("fdsgsfdgs", asList(1, 3, 4, 6, 122));
        addTicket("onotole", asList(12, 3, 4, 632, 12));
    }

    public void addTicket(String number, List<Integer> numbers) {
        for(Integer value: numbers){
            mapStorage.add(number, value);
        }
    }
}
