package com.levelp.jb2.number;

import com.levelp.jb2.util.InputUtil;

public class Factorial {

    public static void main(String[] args) {
        int a = InputUtil.nextInt();
        int factorial = a;
        for (int i = factorial - 1; i > 0; i--) {
            factorial = factorial * i;
        }
        System.out.println(factorial);
    }
}
