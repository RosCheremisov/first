package com.levelp.jb2.number;

/**
 * задача 3 - найти максимальный элемент в массиве из чисел [1, ,5 ,2 ,3, 4, 4,2,3,4,2,1]
 */
public class MaxItem {
    public static void main(String[] args) {
        int[] array = {1, 5, 2, 3, 4, 4, 2, 3, 4, 2, 1};
        int max = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }
        System.out.println(max);
    }
}
