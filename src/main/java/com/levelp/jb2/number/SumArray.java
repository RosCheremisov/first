package com.levelp.jb2.number;

/**
 * задача 3 - посчитать сумму цифр массива [4,5,2,65,23,2,54,]
 */

public class SumArray {
    public static void main(String[] args) {
        int[] number = {4, 5, 2, 65, 23, 2, 54};
        int sum = 0;
        for (int i = 0; i < number.length; i++) {
            sum = sum + number[i];
        }
        System.out.println(sum);

    }
}
