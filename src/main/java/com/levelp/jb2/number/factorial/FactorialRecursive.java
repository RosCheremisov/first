package com.levelp.jb2.number.factorial;

public class FactorialRecursive {
    public static void main(String[] args) {
        FactorialRecursive a = new FactorialRecursive();
        System.out.println(a.factorial(5));
    }


    public int factorial(int a) {
        if (a == 0) {
            return 0;
        }
        if (a == 1) {
            return 1;
        } else {
            return a * factorial(a - 1);
        }
    }
}
