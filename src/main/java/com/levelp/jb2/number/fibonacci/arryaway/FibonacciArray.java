package com.levelp.jb2.number.fibonacci.arryaway;

import com.levelp.jb2.util.InputUtil;

import java.util.Arrays;

public class FibonacciArray {
    public static void main(String[] args) {
        int size = InputUtil.nextInt("Введите размер массива");
        int[] fibonacciArray = new int[size];
        fibonacciArray[0] = 0;
        fibonacciArray[1] = 1;
        for (int i = 2; i < size; i++) {
            fibonacciArray[i] = fibonacciArray[i - 1] + fibonacciArray[i - 2];
        }
        System.out.println(Arrays.toString(fibonacciArray));
        int searchItem = InputUtil.nextInt("Введи позицию числа: ");
        for (int i = 0; i < size; i++) {
            if (searchItem == i) {
                System.out.println(fibonacciArray[i]);
            }
        }
    }
}

