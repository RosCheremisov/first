package com.levelp.jb2.number.fibonacci.arryaway;

public class FibonacciRecursive {
    public static void main(String[] args) {
        FibonacciRecursive a = new FibonacciRecursive();
        System.out.println(a.fibonacci(10));
    }

    public int fibonacci(int a) {
        if (a == 0) {
            return 0;
        }
        if (a == 1) {
            return 1;
        } else {
            return fibonacci(a - 1) + fibonacci(a - 2);
        }
    }
}
