package com.levelp.jb2.number.binary;

import com.levelp.jb2.util.InputUtil;

public class BinarySearchingRecursive {
    public static void main(String[] args) {
        BinarySearchingRecursive a = new BinarySearchingRecursive();
        int searchElement = InputUtil.nextInt("Введите число поиска: ");
        int[] array = {1, 2, 5, 6, 8, 12, 14};
        System.out.println(a.search(array,1,9,searchElement));
    }

    public int search(int [] array, int firstIndex, int lastIndex, int searchElement){
        int middle = (firstIndex + lastIndex) / 2;
        if(searchElement < array[middle]){
            return search(array,firstIndex,middle - 1, searchElement);
        }if(searchElement > array[middle]){
            return search(array,middle+1,lastIndex,searchElement);
        }else if(searchElement == middle){
            return middle;
        }
        return middle;
    }
}
