package com.levelp.jb2.number.binary;

import com.levelp.jb2.util.InputUtil;

public class BinarySearchingLine {
    public static void main(String[] args) {
        BinarySearchingLine a = new BinarySearchingLine();
        int searchElement = InputUtil.nextInt("Введите число для поиска: ");
        int[] sortArray = {1, 2, 5, 6, 8, 12, 14};
        System.out.println(a.searchElement(sortArray, searchElement));

    }

    public int searchElement(int[] array, int searchElement) {
        int firstIndex = 0;
        int lastIndex = array.length - 1;
        while (firstIndex <= lastIndex) {
            int middle = (firstIndex + lastIndex) / 2;
            if (array[middle] < searchElement) {
                firstIndex = middle + 1;
            }else if (array[middle] > searchElement) {
                lastIndex = middle - 1;
            } else {
                return middle;
            }
        }
        return -1;
    }
}
