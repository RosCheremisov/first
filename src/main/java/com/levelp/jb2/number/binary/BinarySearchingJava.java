package com.levelp.jb2.number.binary;

import java.util.Arrays;

public class BinarySearchingJava {
    public static void main(String[] args) {
        int[] array = {2, 1, 5, 4, 9, 7, 6, 82, 71, 32, 14};
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.binarySearch(array,32));
    }

}
