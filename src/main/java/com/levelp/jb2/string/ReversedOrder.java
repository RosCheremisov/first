package com.levelp.jb2.string;

/**
 * На входе строка - записать строку в обратном порядке
 */
public class ReversedOrder {
    public static void main(String[] args) {
        String name = "America";
        for (int i = name.length() - 1; i >= 0; i--) {
            System.out.print(name.charAt(i));
        }
    }
}
