package com.levelp.jb2.string;

/**вывести все индексы символов (char) в строке, позиция
 * которыех всегда четная - 2,4,6 и тд) строку выбрать самому
 *
 */

import com.levelp.jb2.util.InputUtil;

public class EvenChar {
    public static void main(String[] args) {
        String str1 = InputUtil.nextString();
        String str2 = "";
        for (int i = 0; i < str1.length(); i++){
            if (i % 2 != 0){
                str2 += str1.charAt(i);
            }
        }
        System.out.println(str2);
    }
}
