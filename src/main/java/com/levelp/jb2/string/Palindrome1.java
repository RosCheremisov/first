package com.levelp.jb2.string;

import com.levelp.jb2.util.InputUtil;

public class Palindrome1 {
    public static void main(String[] args) {
        String word = InputUtil.nextString();
        String wordRev = "";
        for (int i = word.length() - 1; i >= 0; i--) {
            wordRev = word.charAt(i) + wordRev;
        }
        if (word.equals(wordRev)) {
            System.out.println("Слово полиндром " + wordRev);
        } else {
            System.out.println("Слово полиндром " + wordRev);
        }

    }
}
