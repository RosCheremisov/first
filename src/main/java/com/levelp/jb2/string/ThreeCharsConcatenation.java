package com.levelp.jb2.string;

import java.util.Scanner;

/**
 * первые 3 символа 1-го слова соединить с последними 3-мя от второго и вывести
 */
public class ThreeCharsConcatenation {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите слово 1: ");
        String str1 = scanner.next();

        System.out.println("Введите слово 2: ");
        String str2 = scanner.next();

//        String str3 = "";
//        for (int i = 0; i < 3; i++) {
//            str3 += str1.charAt(i);
//        }
//        for (int j = str2.length() - 1; j > str2.length() - 4; j--) {
//            str3 += str2.charAt(j);
//        }
        String firstPart = str1.length() >= 3 ? str1.substring(0, 2) : str1;
        String lastPart = str2.length() >= 3 ? str1.substring(str2.length() - 4) : str2;
        System.out.println(firstPart + lastPart);
    }
}
