package com.levelp.jb2.string;

public class SalaryList {

    public static void main(String[] args) {
        int monthTotal = 18;
        int salary = 800;
        int step = 200;
        for (int i = 0; i < monthTotal; ++i) {
            System.out.println(i + 1 + " - " + salary);
            if (i % 4 == 0) {
                salary += step;
            }

        }

    }
}
