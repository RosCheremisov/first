package com.levelp.jb2.string;

import com.levelp.jb2.util.InputUtil;

public class ThreeLetters {
    public static void main(String[] args) {
        String str1 = InputUtil.nextString();
        String str2 = InputUtil.nextString();
        String str3 = "";
        for (int i = 0; i < 3; i++) {
            str3 += str1.charAt(i);
        }
        for (int j = str2.length() - 1; j > str2.length() - 4; j--) {
            str3 += str2.charAt(j);
        }
        System.out.println(str3);
    }
}
