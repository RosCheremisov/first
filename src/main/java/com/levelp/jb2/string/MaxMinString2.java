package com.levelp.jb2.string;

import com.levelp.jb2.util.InputUtil;

public class MaxMinString2 {
    public static void main(String[] args) {
        String str1 = InputUtil.nextString();
        String str2 = InputUtil.nextString();
        String str3 = InputUtil.nextString();
        String str4 = InputUtil.nextString();
        String str5 = InputUtil.nextString();
        String max = "";
        String min = "";
        if (str1.length() > str2.length()) {
            max = str1;
            min = str2;
        } else if (str1.length() < str2.length()) {
            max = str2;
            min = str1;
        }
        if (max.length() < str3.length()) {
            max = str3;
        } else if (min.length() >= str3.length()) {
            min = str3;
        }
        if (max.length() < str4.length()) {
            max = str4;
        } else if (min.length() > str4.length()) {
            min = str4;
        }
        if (max.length() < str5.length()) {
            max = str3;
        } else if (min.length() > str5.length()) {
            min = str5;
        }
        System.out.println("Максимальное :" + max);
        System.out.println("Минимальное :" + min);
    }
}
