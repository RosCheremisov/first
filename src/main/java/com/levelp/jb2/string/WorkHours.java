package com.levelp.jb2.string;

import com.levelp.jb2.util.InputUtil;

import java.util.Random;

/**
 * Дана строка на входе - может быть много значений, но мы обробатываем только 3 - "WORK", "REST", "SLEEP". В массиве из 31(дней)
 * нужно считывать число случайное (использовать класс Random) и нужно суммировать эти числа(часы) отдельно для каждого типа строки.
 * В итоге нам нужно вывести суммарно сумму часов, которые мы работали, которую отдыхали и которую спали.
 */

public class WorkHours {
    public static void main(String[] args) {
        int timeWork = 0;
        int timeSleep = 0;
        int timeRest = 0;
        for (int i = 0; i < 8; i++) {
            String value = InputUtil.nextString();
            int nextValue = new Random().nextInt(12);
            if (value.equals("w")) {
                timeWork = nextValue + timeWork;
            }

            if (value.equals("s")) {
                timeSleep = nextValue + timeSleep;
            }
            if (value.equals("r")) {
                timeRest = nextValue + timeRest;
            }
        }
        System.out.println("Работал " + timeWork);
        System.out.println("Спал " + timeSleep);
        System.out.println("Отдыхал " + timeRest);

    }
}
