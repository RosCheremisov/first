package com.levelp.jb2.string;

import com.levelp.jb2.util.InputUtil;

public class Palindrome2 {
    public static void main(String[] args) {
        String word = InputUtil.nextString();
        int j = word.length() - 1;
        boolean isPolindrome = true;
        for (int i = 0; i < word.length() / 2; i++) {
            if (word.charAt(i) != word.charAt(j)) {
                isPolindrome = false;
                break;
            }
            j--;
        }
        System.out.println(isPolindrome + " полиндром ");

    }
}

