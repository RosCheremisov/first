package com.levelp.jb2.string;

/**
 * На входе массив из учеников - ["Ivan", "Oleg", "Petr", "Dima", "Anton"] - вывести в консоль всех учеников у кого
 * в имени встречается буква a или p в  независимо от кейса(case insensitive)
 */
public class Students {
    public static void main(String[] args) {
        String[] names = {"Ivan", "Oleg", "Pert", "Dima", "Anton"};
        for (String str : names) {
            if (str.toLowerCase().contains("a") || str.toLowerCase().contains("p")) {
                System.out.println(str);
            }
        }
    }
}
