package com.levelp.jb2.jackson.json.model;

import lombok.Data;

@Data
public class Details {
    private String displayAspectRatio;
    private String audioConnector;
}
