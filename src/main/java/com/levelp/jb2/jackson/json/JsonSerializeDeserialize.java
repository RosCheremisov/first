package com.levelp.jb2.jackson.json;

import java.io.IOException;

public class JsonSerializeDeserialize {
    public static void main(String[] args) throws IOException {
        JsonService service = new JsonService();
        service.deserialize(service.serialize());
    }
}
