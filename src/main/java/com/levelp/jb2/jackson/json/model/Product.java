package com.levelp.jb2.jackson.json.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
public class Product {

    private String name;
    private String category;
    private Details details;

}
