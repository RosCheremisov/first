package com.levelp.jb2.jackson.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.levelp.jb2.jackson.json.model.Product;
import com.levelp.jb2.util.InputUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class JsonService {
    private final ObjectMapper objectMapper = new ObjectMapper();

    public Product serialize() throws IOException {
        String linkFile = "/phone_product.json";
        InputStream jsonFile = Product.class.getResourceAsStream(linkFile);
        return objectMapper.readValue(jsonFile,Product.class);
    }

    public void deserialize(Product product) throws IOException {
        File outputFile = new File("./src/main/resources/tv_product.json");
        objectMapper.writeValue(outputFile, product);
    }
}

