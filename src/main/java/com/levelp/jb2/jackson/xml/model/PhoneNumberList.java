package com.levelp.jb2.jackson.xml.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.List;
import lombok.Data;

@Data
public class PhoneNumberList {

    @JacksonXmlElementWrapper(localName = "PhoneNumber", useWrapping = false)
    @JacksonXmlProperty(localName = "PhoneNumber")
    private List<PhoneNumber> numberList;
}
