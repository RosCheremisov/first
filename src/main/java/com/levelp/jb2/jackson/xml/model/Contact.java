package com.levelp.jb2.jackson.xml.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

@Data
public class Contact {

    @JacksonXmlProperty(localName = "Name")
    private String name;

    @JacksonXmlProperty(localName = "HomePhone")
    private PhoneNumberList homePhone;

    @JacksonXmlProperty(localName = "WorkPhone")
    private PhoneNumberList workPhone;
}
