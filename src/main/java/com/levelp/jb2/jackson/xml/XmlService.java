package com.levelp.jb2.jackson.xml;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.levelp.jb2.jackson.xml.model.Contact;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class XmlService {
    private final XmlMapper xmlMapper = new XmlMapper();

    public Contact serialize() throws IOException {
        InputStream xmlFile = Contact.class.getResourceAsStream("/wolverine_contact.xml");
        return xmlMapper.readValue(xmlFile,Contact.class);
    }

    public void deserialize(Contact contact) throws IOException {
        File xmlFile = new File("./src/main/resources/simple_contact.xml");
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(xmlFile,contact);
    }
}
