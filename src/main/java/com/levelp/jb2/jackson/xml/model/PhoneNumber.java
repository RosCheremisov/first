package com.levelp.jb2.jackson.xml.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.xml.bind.annotation.XmlElement;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class PhoneNumber {

    @XmlElement(name = "AreaCode", nillable = true)
    @JsonProperty("AreaCode")
    private String areaCode;

    @XmlElement(name = "Number", nillable = true)
    @JsonProperty("Number")
    private String number;
}
