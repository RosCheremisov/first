package com.levelp.jb2.jackson.xml;

import com.levelp.jb2.jackson.xml.model.Contact;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import java.io.*;

@Log
@Slf4j
public class XmlSerializeDeserialize {
    public static void main(String[] args) throws IOException {
        XmlService service = new XmlService();
        Contact contact = service.serialize();
        service.deserialize(contact);
        log.info("Новый контак :" + contact );
    }
}
