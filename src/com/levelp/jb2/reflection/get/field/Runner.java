package com.levelp.jb2.reflection.get.field;
import java.lang.reflect.Field;

/**
 * Создать класс со статической приватной переменной - из другого класса через Reflection поменять значение этой переменной
 */

public class Runner {

    public static void main(String[] args) {

        getFieldClass(SimpleClass.class);

    }

    public static void getFieldClass(Class<?> clazz){
        int number = 0;
        System.out.println(number);
        try{
            Field field = clazz.getDeclaredField("number");
            field.setAccessible(true);
            field.set(clazz, 5);
            number = (int) field.get(clazz);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println(number);
    }
}
