package com.levelp.jb2.reflection.get.field;

import lombok.AllArgsConstructor;
import lombok.Setter;

@Setter
@AllArgsConstructor
public class SimpleClass {

    private static int number = 1;

}
