package com.levelp.jb2.reflection.get.method;
/**
 * Через рефлексию вывести все приватные методы класса. Создать модель с несколькими полями и разными функциями
 */

import java.lang.reflect.Method;

public class Runner {

    public static void main(String[] args) {
        getMethodsClass(SimpleClass.class);
    }

    public static void getMethodsClass(Class<?> clazz){
        Method[] methods = clazz.getDeclaredMethods();
        for(Method method: methods){
            method.setAccessible(true);
            System.out.println(method);
        }
    }
}
