package com.levelp.jb2.anotation;

@Service(name = "NotBadService")
public class SimpleService {
    @Init
    public void initService(){
        System.out.println("Init Service");
    }

    public void anotherService(){
        System.out.println("Another");
    }
}
