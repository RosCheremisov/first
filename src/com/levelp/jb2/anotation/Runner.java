package com.levelp.jb2.anotation;

/**
 * задание по обучающему уроку , вызов мвсех методов с помощью рефлексии
 */

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class Runner {
    static Map<String,Object> serviceMap = new HashMap<>();
    public static void main(String[] args) {
        try {
            loadService("com.levelp.jb2.anotation.LazyService");
            loadService("com.levelp.jb2.anotation.LazyService");
            loadService("java.lang.String");
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    public static void loadService(String className) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class<?> clazz = Class.forName(className);
        if(clazz.isAnnotationPresent(Service.class)){
            Object serviceObj = clazz.newInstance();
            serviceMap.put(className,serviceObj);
            Method[] methods = clazz.getMethods();
            for(Method method: methods){
                if(method.isAnnotationPresent(Init.class)){
                    try{
                        method.invoke(serviceObj);
                    }
                    catch (Exception e){
                        Init ann = method.getAnnotation(Init.class);
                        if(ann.suppressException()){
                            System.err.println(e.getMessage());
                        }
                        else{
                            throw new RuntimeException();
                        }
                    }
                }
            }
        }
    }
}
